#########################################################
# Virtualenvwrapper configuration and initialization    #
#########################################################
## virtualenvwrapper configuration

# Configure the Python interpreter to be used by virtualenvwrapper
export VIRTUALENVWRAPPER_PYTHON=`which python3`
# root dir for the virtual environments managed by virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
# root dir for the projects managed by virtualenvwrapper
export PROJECT_HOME=$HOME/dev
# store "hook loader" logs into `$WORKON_HOME`
export VIRTUALENVWRAPPER_LOG_FILE=$WORKON_HOME/hooks.log

## pip-virtualenv(wrapper) integration

# tell pip to use the same parent dir for virtual environments
# as virtualenvwrapper
export PIP_VIRTUALENV_BASE=$WORKON_HOME
# tell pip to install distributions on the currently active virtualenv (if one) 
export PIP_RESPECT_VIRTUALENV=true

# initialize  virtualenvwrapper
source `which virtualenvwrapper.sh`

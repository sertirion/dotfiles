# ~/.bash_logout: executed by bash(1) when login shell exits.

# when leaving the console clear the screen to increase privacy

if [ "$SHLVL" = 1 ]; then
    [ -x /usr/bin/clear_console ] && /usr/bin/clear_console -q
fi

# Automatically invoke `deactivate` if a virtual environment is active when exiting the shell
[ "$VIRTUAL_ENV" ] && deactivate
